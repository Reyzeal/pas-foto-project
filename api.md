# Pas Foto API

Base url : `http://139.180.209.183/api`

### Sending Photo

Store raw photo to server through HTTP POST request. Image data must be encoded with base 64 encoding.
#### POST `/send`
```
{
    'photo' : 'base64 encoding string'
}
```
#### Success
Field `url` use as token to identifiy which photo is being process.
```
{
    'error' : false,
    'data'	: {
        "type": "raw",
        "path": "temp/9k89UGSnsS",
        "real_path": "/var/www/html/pas-foto/storage/app/temp/9k89UGSnsS.jpeg",
        "url": "259772ad9a4eb9db9a30fbc91f2b3b3a",
        "updated_at": "2018-11-15 05:13:58",
        "created_at": "2018-11-15 05:13:58",
        "id": 37
    }
}
```
#### Failed
##### String isn't base 64
```
{
    'error' 	: false,
    'data'	: "isn't base64"
}
```
##### No field `photo`
```
{
    'error' 	: false,
    'data'		: "invalid format"
}
```
### Processing Photo `/proceed/{url_token}`
String `url_token` is data field `url` from `/send` action. Send data color as hex encoding. Example `#ff0000` for red color background.
```
{
    'color' : '#f96000'
}
```

#### Success
```
{
	"error": false,
	"data": {
		"mask": "cb687c2eeca2a1788c7d43db9f53ecac",
		"result": "dcca6c1f6bc76610b7d321e6421938ca"
	}
}
```
#### Failed
Problem may occur when md5 hash file token doesn't match in database. Please check the string from 
```
{
	"error": true,
	"data": "Photo not found"
}
```
#### Failed 2
Problem may occur when processing error, like failed to detect face or else. 
```
{
	"error": true,
	"data": "string error from opencv"
}
```
### Mask Revision
```
{
    'color' : '#f96000',
    'mask'	: base64 string
}
```
#### Success
```
{
	"error": false,
	"data": {
		"mask": "cb687c2eeca2a1788c7d43db9f53ecac",
		"result": "dcca6c1f6bc76610b7d321e6421938ca"
	}
}
```